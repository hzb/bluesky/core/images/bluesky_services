# Bluesky Services

The repository consists of services needed to run Bluesky for a beamline. The following components are included in the stack:

- **Tiled**: An API for managing and interacting with data streams.
- **MongoDB**: A NoSQL database for storing configuration data and metrics.
- **ZMQ Proxy**: A proxy for the Bluesky 0MQ interface.
- **Tiled Writing Callback**: A service for sending messages to Tiled when a new run is started.

## How to Run the Services

To run the services, follow these steps:

1. **Define Environment Variables:**
   Add the following variable to your `~/.bashrc` file and then run `source ~/.bashrc` to apply the changes:

   ```bash
   export MONGO_DATA_LOCATION=<mong-db-data-location>
   export BEAMLINE_USER_UID=<beamline-user-uid>
   export TILED_API_KEY=<tiled-api-key>
   ```

2. **Start the Services:**
   Use Docker Compose to start all the services defined in the `docker-compose.yml` file:

   ```bash
   docker-compose up -d
   ```
